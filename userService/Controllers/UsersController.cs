﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using userService.Models;

namespace userService.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        Random rnd = new Random();
        public List<User> _userContext = new List<User>();

        public UsersController()
        {
            _userContext.Add(new User() {
                Id = 123456,
                FirstName = "Tyler",
                LastName = "Sully",
                UserName = "sidx1",
                Email = "sidx1@gmail.com",
                Password = "pw",
                ConfirmPassword = "pw"  
            });
            _userContext.Add(new User() {
                Id = 123457,
                FirstName = "Jimmy",
                LastName = "Fallon",
                UserName = "fallonBro89",
                Email = "fallonBro89@gmail.com",
                Password = "pw",
                ConfirmPassword = "pw"  
            });
        }
        
        // GET api/values
        [HttpGet]
        public IEnumerable<User> GetAll()
        {   
            return _userContext;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public User Get(long id)
        {
            return _userContext.Find(t => t.Id == id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
